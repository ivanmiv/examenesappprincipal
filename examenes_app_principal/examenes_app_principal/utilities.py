from django.conf import settings
from django.contrib import messages


from django.shortcuts import redirect

# Decorador que verifica que el usuario tenga alguno de los roles permitidos
def verificar_rol(roles_permitidos):
    def _method_wrapper(view_method):
        def _arguments_wrapper(request, *args, **kwargs):
            request_inicial = request
            if hasattr(request, "request"):  # Es una CBV
                request = request.request

            try:
                rol = request.session[settings.MICROSERVICE_USER_SESSION_KEY]['rol']
                if not (rol in roles_permitidos):
                    messages.error(request, "Usted no tiene ninguno de los roles permitidos para acceder a la página solicitada")
                    request.session['_next_login'] = request.path
                    return redirect('usuarios:login')
            except KeyError:
                messages.error(request, "Para acceder a la página solicitada requiere loguearse")
                request.session['_next_login'] = request.path
                return redirect('usuarios:login')

            return view_method(request_inicial, *args, **kwargs)

        return _arguments_wrapper

    return _method_wrapper

def add_form_errors(form, errors):
    for field, error in errors.items():
        if field in form.fields:
            form.add_error(field, error)
        elif field == 'non_field_errors':
            form.add_error(None, error)
