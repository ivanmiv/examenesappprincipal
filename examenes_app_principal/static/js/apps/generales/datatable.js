/*
Template Name: Color Admin - Responsive Admin Dashboard Template build with Twitter Bootstrap 3 & 4
Version: 4.1.0
Author: Sean Ngu
Website: http://www.seantheme.com/color-admin-v4.1/admin/
*/

var handleDataTableCombinationSetting = function() {
	"use strict";
	var $data_combine = $('.data-combine');
    if ($data_combine.length !== 0) {
        $data_combine.DataTable({
            dom: 'lBfrtip',
            buttons: [
                { extend: 'excel', className: 'btn-sm' },
                { extend: 'pdf', className: 'btn-sm' },
                { extend: 'print', className: 'btn-sm' }
            ],
            rowReorder: true,
            "language": {
                "url": static_url+"plugins/DataTables/media/i18n/Spanish.json"
            },
            order:[]
        });
    }
};

var TableManageCombine = function () {
	"use strict";
    return {
        //main function
        init: function () {
            handleDataTableCombinationSetting();
        }
    };
}();