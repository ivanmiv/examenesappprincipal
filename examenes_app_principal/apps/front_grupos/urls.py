from django.urls import path

from .views import *

app_name = 'grupos'
urlpatterns = [
    path('registrar', registrar_grupo, name='registrar_grupo'),
    path('modificar/<int:pk>', modificar_grupo, name='modificar_grupo'),
    path('listar', consultar_grupos, name='consultar_grupos'),
    path('listar-grupos-estudiante', consultar_grupos_estudiante, name='consultar_grupos_estudiante'),
]
