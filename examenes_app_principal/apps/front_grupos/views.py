from django.contrib import messages
from django.shortcuts import render, redirect
from django_microservices.utilities import get_microservice_user_pk, get_microservice_user
from django_microservices.utilities import login_required
from rest_framework import status

from examenes_app_principal.utilities import verificar_rol, add_form_errors
from .forms import *


@login_required
@verificar_rol(roles_permitidos=["Profesor"])
def registrar_grupo(request):
    microservicio = MicroService.objects.get(name="api")
    profesor = get_microservice_user(request)
    form = GrupoForm(profesor=profesor)

    if request.method == 'POST':
        form = GrupoForm(request.POST, profesor=profesor)
        if form.is_valid():
            try:
                data = form.cleaned_data
                data.update({'profesor': get_microservice_user_pk(request)})
                respuesta = microservicio.remote_call(
                        "POST", api='api/grupos/', data=data, request=request
                )
            except Exception as e:
                messages.error(request, 'Ha ocurrido un error al guardar la información, por favor vuelva a intentar.')
                return redirect('grupos:consultar_grupos')

            if respuesta.status_code == status.HTTP_201_CREATED:
                messages.success(request, "Grupo registrado correctamente")
                return redirect('grupos:consultar_grupos')
            elif respuesta.status_code == status.HTTP_400_BAD_REQUEST:
                errores = respuesta.json()
                form = GrupoForm(request.POST, profesor=profesor)
                add_form_errors(form, errores)
                messages.error(request, "Por favor revise los campos en rojo")
                return render(request, "gestionar_grupo.html", {'form': form})
            else:
                messages.error(request, "Ha ocurrido un error al guardar la información, por favor vuelva a intentar.")
                return render(request, "gestionar_grupo.html", {'form': form})
        else:
            messages.error(request, "Por favor revise los campos en rojo")
            return render(request, "gestionar_grupo.html", {'form': form})

    return render(request, "gestionar_grupo.html", {'form': form})


@login_required
@verificar_rol(roles_permitidos=["Profesor"])
def modificar_grupo(request, pk):
    microservicio = MicroService.objects.get(name="api")
    profesor = get_microservice_user(request)

    try:
        respuesta = microservicio.remote_call(
                "GET", api='api/grupos/%s' % pk, request=request
        )
    except Exception:
        messages.error(request, 'Ha ocurrido un error, por favor vuelva a intentar.')
        return redirect('inicio')

    if respuesta.status_code == 200:
        grupo = respuesta.json()
    else:
        messages.error(request, 'Error al obtener los datos del grupo')
        return redirect('grupos:consultar_grupos')

    form = GrupoForm(initial=grupo, profesor=profesor)

    if request.method == 'POST':
        form = GrupoForm(request.POST, initial=grupo, profesor=profesor)
        if form.is_valid():
            try:
                data = dict(request.POST)
                respuesta = microservicio.remote_call(
                        "POST", api='api/grupos/%s' % pk, data=data, request=request
                )
            except Exception as e:
                messages.error(request, 'Ha ocurrido un error al guardar la información, por favor vuelva a intentar.')
                return redirect('grupos:consultar_grupos')

            if respuesta.status_code == status.HTTP_200_OK:
                messages.success(request, "Grupo modificado correctamente")
                return redirect('grupos:consultar_grupos')
            elif respuesta.status_code == status.HTTP_400_BAD_REQUEST:
                errores = respuesta.json()
                add_form_errors(form, errores)
                messages.error(request, "Por favor revise los campos en rojo")
                return render(request, "gestionar_grupo.html", {'form': form})
        else:
            messages.error(request, "Por favor revise los campos en rojo")
            return render(request, "gestionar_grupo.html", {'form': form})
    return render(request, "gestionar_grupo.html", {'form': form})


@login_required
def consultar_grupos(request):
    datos = []
    microservicio = MicroService.objects.get(name="api")
    try:
        respuesta = microservicio.remote_call(
                "GET", api='api/grupos/', request=request
        )
    except Exception as e:
        messages.error(request, 'Ha ocurrido un error, por favor vuelva a intentar.')
        return redirect('inicio')

    if respuesta.status_code == 200:
        grupos = respuesta.json()
    else:
        messages.error(request, 'Ha ocurrido un error, por favor vuelva a intentar.')
        return redirect('inicio')
    return render(request, "lista_grupos.html", {'lista_grupos': grupos})


@login_required
def consultar_grupos_estudiante(request):
    microservicio = MicroService.objects.get(name="api")
    try:
        respuesta = microservicio.remote_call(
                "GET", api='api/grupos/', request=request
        )
    except Exception as e:
        messages.error(request, 'Ha ocurrido un error, por favor vuelva a intentar.')
        return redirect('inicio')

    if respuesta.status_code == 200:
        grupos = respuesta.json()
    else:
        messages.error(request, 'Ha ocurrido un error, por favor vuelva a intentar.')
        return redirect('inicio')
    return render(request, "lista_grupos_estudiante.html", {'lista_grupos': grupos})
