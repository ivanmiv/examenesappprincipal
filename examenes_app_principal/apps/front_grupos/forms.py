from django import forms

from django_microservices.models import MicroService
from django_select2.forms import Select2MultipleWidget


class GrupoForm(forms.Form):
    nombre = forms.CharField(max_length=250, label="Nombre del grupo")
    codigo = forms.CharField(max_length=150, label="Código del grupo (opcional)", required=False)
    numero = forms.CharField(max_length=50, label="Número del grupo (opcional)", required=False)
    estudiantes = forms.MultipleChoiceField(label="Estudiantes", widget=Select2MultipleWidget(), required=False)

    def __init__(self, *args, **kwargs):
        profesor = kwargs.pop('profesor', {})
        super(GrupoForm, self).__init__(*args, **kwargs)

        try:
            microservicio = MicroService.objects.get(name="api")
            respuesta = microservicio.remote_call(
                    "GET", api='api/usuarios/', headers={'MICROSERVICE-USER-PK': str(profesor['id'])}
            )

            if respuesta.status_code == 200:
                estudiantes = []
                for estudiante in respuesta.json():
                    if estudiante['rol'] == "Estudiante":
                        estudiantes.append((estudiante['id'], "%s %s" % (estudiante['first_name'], estudiante['last_name'])))
            else:
                estudiantes = []
        except Exception as e:
            estudiantes = []

        self.fields['estudiantes'].choices = estudiantes
