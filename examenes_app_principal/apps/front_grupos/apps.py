from django.apps import AppConfig


class FrontGruposConfig(AppConfig):
    name = 'front_grupos'
