from django.urls import path

from .views import *

app_name = 'usuarios'
urlpatterns = [
    path('registrar', registrar_usuario, name='registrar_usuario'),
    path('modificar/<int:pk>', modificar_usuario, name='modificar_usuario'),
    path('listar', consultar_usuarios, name='consultar_usuarios'),
    path('detalle/<int:pk>', detalle_usuario, name='detalle_usuario'),
    path('login', Login.as_view(), name='login'),
    path('logout', Logout, name='logout'),
]
