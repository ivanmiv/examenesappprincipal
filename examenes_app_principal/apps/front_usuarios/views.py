from django.contrib import messages
from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.views.generic import FormView
from django_microservices.models import MicroService
from django_microservices.utilities import authenticate, login, logout, login_required
from rest_framework import status

from examenes_app_principal.utilities import verificar_rol
from .forms import *


@login_required
@verificar_rol(roles_permitidos=["Administrador"])
def registrar_usuario(request):
    form = RegistrarUsuarioForm()

    microservicio = MicroService.objects.get(name="api")

    if request.method == 'POST':
        try:
            respuesta = microservicio.remote_call(
                    "POST", api='api/usuarios/', data=request.POST, files=request.FILES, request=request
            )
        except Exception:
            messages.error(request, 'Ha ocurrido un error al guardar la información, por favor vuelva a intentar.')
            return redirect('usuarios:consultar_usuarios')

        form = RegistrarUsuarioForm(request.POST, request.FILES)
        if respuesta.status_code == status.HTTP_201_CREATED:
            return redirect('usuarios:consultar_usuarios')
        elif respuesta.status_code == status.HTTP_400_BAD_REQUEST:
            errores = respuesta.json()
            form = RegistrarUsuarioForm(request.POST, request.FILES)
            for campo, error in errores.items():
                form.add_error(campo, error)

            return render(request, "registrar_usuario.html", {'form': form})

    return render(request, "registrar_usuario.html", {'form': form})


@login_required
@verificar_rol(roles_permitidos=["Administrador"])
def modificar_usuario(request, pk):
    microservicio = MicroService.objects.get(name="api")
    try:
        respuesta = microservicio.remote_call(
                "GET", api='api/usuarios/%s' % pk, request=request
        )
    except Exception:
        messages.error(request, 'Ha ocurrido un error, por favor vuelva a intentar.')
        return redirect('inicio')

    if respuesta.status_code == 200:
        usuario = respuesta.json()
    else:
        messages.error(request, 'Error al obtener los datos del usuario')
        return redirect('usuarios:consultar_usuarios')

    form = ModificarUsuarioForm(initial=usuario)

    if request.method == 'POST':
        try:
            respuesta = microservicio.remote_call(
                    "POST", api='api/usuarios/%s' % usuario['id'], data=request.POST, files=request.FILES, request=request
            )
        except Exception:
            messages.error(request, 'Ha ocurrido un error al guardar la información, por favor vuelva a intentar.')
            return redirect('inicio')

        form = ModificarUsuarioForm(request.POST, request.FILES, initial=usuario)
        if respuesta.status_code == status.HTTP_200_OK:
            return redirect('usuarios:consultar_usuarios')
        elif respuesta.status_code == status.HTTP_400_BAD_REQUEST:
            errores = respuesta.json()
            for campo, error in errores.items():
                form.add_error(campo, error)

            return render(request, "modificar_usuario.html", {'form': form})
        else:
            messages.error(request, 'Ha ocurrido un error al guardar la información, por favor vuelva a intentar.')
            return redirect('usuarios:consultar_usuarios')

    return render(request, "modificar_usuario.html", {'form': form})


@login_required
def consultar_usuarios(request):
    microservicio = MicroService.objects.get(name="api")
    try:
        respuesta = microservicio.remote_call(
                "GET", api='api/usuarios/', request=request
        )
    except Exception:
        messages.error(request, 'Ha ocurrido un error, por favor vuelva a intentar.')
        return redirect('inicio')

    if respuesta.status_code == 200:
        usuarios = respuesta.json()
    else:
        return HttpResponse(respuesta.status_code)

    return render(request, "lista_usuarios.html", {'lista_usuarios': usuarios})


@login_required
def detalle_usuario(request, pk):
    microservicio = MicroService.objects.get(name="api")
    try:
        respuesta = microservicio.remote_call(
                "GET", api='api/usuarios/%s' % pk, request=request
        )
    except Exception:
        messages.error(request, 'Ha ocurrido un error, por favor vuelva a intentar.')
        return redirect('usuarios:consultar_usuarios')

    if respuesta.status_code == 200:
        usuario = respuesta.json()
    else:
        messages.error(request, "Ha ocurrido un error, por favor vuelva a intentar.")
        return redirect('usuarios:consultar_usuarios')

    return render(request, "detalle_usuario.html", {'usuario': usuario})


@login_required
def inicio(request):
    return render(request, 'base.html')


class Login(FormView):
    form_class = LoginForm
    template_name = 'login.html'
    success_url = reverse_lazy('inicio')

    def form_valid(self, form):
        try:
            usuario = authenticate(username=form.cleaned_data['username'], password=form.cleaned_data['password'])
        except Exception:
            messages.error(self.request, "Ha ocurrido un error, por favor vuelva a intentarlo")
            return super(Login, self).form_invalid(form)

        if usuario is not None:
            # if usuario.is_active:
            login(self.request, usuario)
            redireccion = self.request.session.pop('_next_login', None)
            if redireccion:
                return redirect(redireccion)
            return super(Login, self).form_valid(form)
        # elif not usuario.is_active:
        #     mensaje = _("El usuario %s no esta activo" % (usuario.username))
        else:
            mensaje = "El usuario no existe o la contraseña es incorrecta"
        form.add_error('username', mensaje)
        messages.add_message(self.request, messages.ERROR, mensaje)
        return super(Login, self).form_invalid(form)

    def form_invalid(self, form):
        messages.add_message(self.request, messages.ERROR, "Ingrese un usuario y una contraseña para acceder al sistema")
        return super(Login, self).form_invalid(form)


def Logout(request):
    logout(request)
    messages.success(request, "Sesión cerrada correctamente")
    return redirect('inicio')
