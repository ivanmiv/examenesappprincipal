from django import forms
from django.contrib.auth import password_validation


class RegistrarUsuarioForm(forms.Form):
    ROLES = (
        ("Administrador", "Administrador"),
        ("Profesor", "Profesor"),
        ("Estudiante", "Estudiante"),
    )

    error_messages = {
        'password_mismatch': "Las contraseñas no coinciden",
    }

    first_name = forms.CharField(max_length=150, label="Nombres")
    last_name = forms.CharField(max_length=150, label="Apellidos")
    username = forms.CharField(max_length=150, label="Nombre de usuario")
    email = forms.CharField(max_length=150, label="Correo electrónico")
    rol = forms.ChoiceField(choices=ROLES)
    imagen_perfil = forms.ImageField(required=False)

    password = forms.CharField(
            label="Contraseña",
            strip=False,
            widget=forms.PasswordInput,
            help_text=password_validation.password_validators_help_text_html(),
    )
    password2 = forms.CharField(
            label="Confirmar contraseña",
            widget=forms.PasswordInput,
            strip=False,
            help_text="Ingrese de nuevo la misma contraseña",
    )

    def clean_password2(self):
        password = self.cleaned_data.get("password")
        password2 = self.cleaned_data.get("password2")
        if password and password2 and password != password2:
            raise forms.ValidationError(
                    self.error_messages['password_mismatch'],
                    code='password_mismatch',
            )
        return password2


class ModificarUsuarioForm(forms.Form):
    ROLES = (
        ("Administrador", "Administrador"),
        ("Profesor", "Profesor"),
        ("Estudiante", "Estudiante"),
    )

    first_name = forms.CharField(max_length=150, label="Nombres")
    last_name = forms.CharField(max_length=150, label="Apellidos")
    username = forms.CharField(max_length=150, label="Nombre de usuario")
    email = forms.CharField(max_length=150, label="Correo electrónico")
    rol = forms.ChoiceField(choices=ROLES)
    imagen_perfil = forms.ImageField(required=False)


class LoginForm(forms.Form):
    username = forms.CharField(max_length=50, label='Nombre de usuario',
                               widget=forms.TextInput(attrs={
                                   'class': 'form-control',
                                   'placeholder': '',
                               }))

    password = forms.CharField(max_length=50, label='Contraseña',
                               widget=forms.TextInput(attrs={
                                   'type': 'password',
                                   'class': 'form-control',
                                   'placeholder': '',
                               }))
