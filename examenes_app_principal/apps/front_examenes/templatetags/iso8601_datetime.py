from django import template

import iso8601
register = template.Library()


@register.filter
def iso8601_datetime(fecha):
    return iso8601.parse_date(fecha)