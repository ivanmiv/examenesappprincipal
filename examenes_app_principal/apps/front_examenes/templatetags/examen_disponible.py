from datetime import timedelta

import iso8601
from django import template
from django.utils import timezone

register = template.Library()


@register.filter
def examen_disponible(examen):
    return iso8601.parse_date(examen['fecha_inicio']) < timezone.now() and timezone.now() < (iso8601.parse_date(examen['fecha_fin']) - timedelta(seconds=1)) and examen['iniciado'] == True and examen['terminado'] == False
