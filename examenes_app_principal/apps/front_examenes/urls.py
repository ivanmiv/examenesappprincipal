from django.urls import path

from .views import *

app_name = 'examenes'
urlpatterns = [
    path('preguntas/registrar', registrar_pregunta, name='registrar_pregunta'),
    path('preguntas/modificar/<int:pk>', modificar_pregunta, name='modificar_pregunta'),
    path('preguntas/listar', consultar_preguntas, name='consultar_preguntas'),
    path('preguntas/detalle/<int:pk>', detalle_pregunta, name='detalle_pregunta'),

    path('registrar', registrar_examen, name='registrar_examen'),
    path('modificar/<int:pk>', modificar_examen, name='modificar_examen'),
    path('listar', consultar_examenes, name='consultar_examenes'),
    path('detalle/<int:pk>', detalle_examen, name='detalle_examen'),

    path('iniciar-examen/<int:pk>', iniciar_examen, name='iniciar_examen'),
    path('terminar-examen/<int:pk>', terminar_examen, name='terminar_examen'),
    path('listar-calificaciones-examen/<int:pk>', consultar_calificaciones_examen, name='consultar_calificaciones_examen'),

    path('listar-examenes-estudiante', consultar_examenes_estudiante, name='consultar_examenes_estudiante'),
    path('listar-examenes-resueltos-estudiante', consultar_examenes_resueltos_estudiante, name='consultar_examenes_resueltos_estudiante'),

    path('responder-examen/<int:pk>', responder_examen, name='responder_examen'),
    path('ver-calificacion-examen/<int:pk>', ver_calificacion, name='ver_calificacion'),
]
