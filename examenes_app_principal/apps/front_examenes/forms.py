from django import forms
from django.forms.formsets import BaseFormSet
from django_microservices.models import MicroService
from django_summernote.widgets import SummernoteWidget


class PreguntaForm(forms.Form):
    enunciado = forms.CharField(widget=SummernoteWidget(attrs={
        'data-parsley-group': 'step-1',
        'data-parsley-required': 'true'
    }))


class RespuestaForm(forms.Form):
    id = forms.IntegerField(required=False)
    respuesta = forms.CharField(widget=forms.Textarea(attrs={
        'rows': 4,
        'style': 'resize:none;',
        'data-parsley-group': 'step-2',
        'data-parsley-required': 'true'
    }))
    imagen = forms.ImageField(required=False)
    es_correcta = forms.BooleanField(required=False)


class ExamenForm(forms.Form):
    titulo = forms.CharField(max_length=255, label='Título')
    grupo = forms.ChoiceField(label="Grupo")
    fecha_inicio = forms.DateTimeField(label="Fecha de inicio")
    fecha_fin = forms.DateTimeField(label="Fecha de finalización")
    preguntas_originales = forms.MultipleChoiceField(label="Preguntas", required=False, widget=forms.CheckboxSelectMultiple())

    def __init__(self, *args, **kwargs):
        profesor = kwargs.pop('profesor', None)
        examen = kwargs.pop('examen', None)
        super(ExamenForm, self).__init__(*args, **kwargs)
        try:
            microservicio = MicroService.objects.get(name="api")
            respuesta = microservicio.remote_call(
                    "GET", api='api/grupos/', headers={'MICROSERVICE-USER-PK': str(profesor['id'])}
            )

            if respuesta.status_code == 200:
                grupos = []
                for grupo in respuesta.json():
                    grupos.append((grupo['id'], grupo['nombre']))
            else:
                grupos = []
        except Exception:
            grupos = []

        self.fields['grupo'].choices = grupos

        try:
            microservicio = MicroService.objects.get(name="api")
            respuesta = microservicio.remote_call(
                    "GET", api='api/examenes/preguntas', headers={'MICROSERVICE-USER-PK': str(profesor['id'])}
            )
            if respuesta.status_code == 200:
                preguntas_originales = []
                for pregunta in respuesta.json():
                    preguntas_originales.append((pregunta['id'], pregunta['enunciado']))
            else:
                preguntas_originales = []
        except Exception as e:
            preguntas_originales = []
        self.fields['preguntas_originales'].choices = preguntas_originales

        if examen:
            self.initial['preguntas_originales'] = [pregunta['id'] for pregunta in examen['preguntas_originales']]


class PreguntaResueltaForm(forms.Form):
    respuestas_seleccionadas = forms.MultipleChoiceField(label="Respuestas", required=False, widget=forms.CheckboxSelectMultiple)
    id = forms.CharField(widget=forms.HiddenInput())

    def __init__(self, *args, **kwargs):
        self.pregunta = kwargs.pop('pregunta', None)
        self.lista_preguntas = kwargs.pop('lista_preguntas', None)

        super(PreguntaResueltaForm, self).__init__(*args, **kwargs)

        self.fields['respuestas_seleccionadas'].choices = [(respuesta['id'], respuesta['respuesta']) for respuesta in self.pregunta.get('respuestas')]

        self.initial['respuestas_seleccionadas'] = [respuesta['id'] for respuesta in self.pregunta.get('respuestas_seleccionadas')]
        self.fields['id'].initial = self.pregunta['id']

class BasePreguntasFormSet(BaseFormSet):
    def get_form_kwargs(self, index):
        kwargs = super(BasePreguntasFormSet, self).get_form_kwargs(index)
        kwargs['pregunta'] = kwargs['lista_preguntas'][index]
        return kwargs
