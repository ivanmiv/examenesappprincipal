from django.apps import AppConfig


class FrontExamenesConfig(AppConfig):
    name = 'front_examenes'
