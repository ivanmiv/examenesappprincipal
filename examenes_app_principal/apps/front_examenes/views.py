from django.conf import settings
from django.contrib import messages
from django.forms import formset_factory
from django.http import JsonResponse, HttpResponseBadRequest
from django.shortcuts import render, redirect
from django_microservices.utilities import get_microservice_user_pk, get_microservice_user
from django_microservices.utilities import login_required
from rest_framework import status

from examenes_app_principal.utilities import verificar_rol, add_form_errors
from .forms import *


@login_required
@verificar_rol(roles_permitidos=["Profesor"])
def registrar_pregunta(request):
    form = PreguntaForm()
    numero_respuestas = 4
    RespuestaFormSet = formset_factory(form=RespuestaForm, extra=numero_respuestas, max_num=numero_respuestas)
    formset_respuestas = RespuestaFormSet()

    microservicio = MicroService.objects.get(name="api")

    if request.method == 'POST':
        form = PreguntaForm(request.POST, request.FILES)
        formset_respuestas = RespuestaFormSet(request.POST, request.FILES)
        try:
            if form.is_valid() and formset_respuestas.is_valid():
                data = form.cleaned_data
                data['creador'] = get_microservice_user_pk(request)
                respuestas = []
                for form_respuesta in formset_respuestas.forms:
                    respuestas.append(form_respuesta.cleaned_data)

                data['respuestas'] = respuestas

                respuesta = microservicio.remote_call(
                        "POST", api='api/examenes/preguntas', json=data, files=request.FILES, request=request
                )
            else:
                messages.error(request, "Por favor revise los campos en rojo")
                return render(request, "registrar_pregunta.html", {'form': form, 'formset_respuestas': formset_respuestas})

        except Exception as e:
            messages.error(request, 'Ha ocurrido un error al guardar la información, por favor vuelva a intentar.')
            return render(request, "registrar_pregunta.html", {'form': form, 'formset_respuestas': formset_respuestas})

        form = PreguntaForm(request.POST, request.FILES)
        formset_respuestas = RespuestaFormSet(request.POST, request.FILES)
        if respuesta.status_code == status.HTTP_201_CREATED:
            messages.success(request, "Pregunta registrada correctamente")
            return redirect('examenes:consultar_preguntas')
        elif respuesta.status_code == status.HTTP_400_BAD_REQUEST:
            errores = respuesta.json()
            form = PreguntaForm(request.POST, request.FILES)
            formset_respuestas = RespuestaFormSet(request.POST, request.FILES)

            add_form_errors(form, errores)
            messages.error(request, "Por favor revise los campos en rojo")
            return render(request, "registrar_pregunta.html", {'form': form, 'formset_respuestas': formset_respuestas})

    return render(request, "registrar_pregunta.html", {'form': form, 'formset_respuestas': formset_respuestas})


@login_required
@verificar_rol(roles_permitidos=["Profesor"])
def modificar_pregunta(request, pk):
    microservicio = MicroService.objects.get(name="api")
    try:
        respuesta = microservicio.remote_call(
                "GET", api='api/examenes/preguntas/%s' % pk, request=request
        )
    except Exception:
        messages.error(request, 'Ha ocurrido un error, por favor vuelva a intentar.')
        return redirect('inicio')

    if respuesta.status_code == 200:
        pregunta = respuesta.json()
    else:
        messages.error(request, 'Error al obtener los datos de la pregunta')
        return redirect('examenes:consultar_preguntas')
    form = PreguntaForm(initial=pregunta)
    resuestas_iniciales = [respuesta for respuesta in pregunta.get('respuestas')]
    numero_respuestas = len(resuestas_iniciales)
    RespuestaFormSet = formset_factory(form=RespuestaForm, extra=numero_respuestas, max_num=numero_respuestas)
    formset_respuestas = RespuestaFormSet(initial=resuestas_iniciales)

    if request.method == 'POST':
        form = PreguntaForm(request.POST, request.FILES, initial=pregunta)
        formset_respuestas = RespuestaFormSet(request.POST, request.FILES, initial=resuestas_iniciales)
        if form.is_valid() and formset_respuestas.is_valid():
            data = form.cleaned_data
            data['creador'] = pregunta['creador']
            data['respuestas'] = formset_respuestas.cleaned_data
        else:
            messages.error(request, "Por favor revise los campos en rojo")
            return render(request, "modificar_pregunta.html", {'form': form, 'formset_respuestas': formset_respuestas})

        try:
            respuesta = microservicio.remote_call(
                    "POST", api='api/examenes/preguntas/%s' % pk, json=data, files=request.FILES, request=request
            )
            if respuesta.status_code == status.HTTP_200_OK:
                messages.success(request, "Pregunta registrada correctamente")
                return redirect('examenes:consultar_preguntas')
            elif respuesta.status_code == status.HTTP_400_BAD_REQUEST:
                errores = respuesta.json()
                add_form_errors(form, errores)
                messages.error(request, "Por favor revise los campos en rojo")
                return render(request, "modificar_pregunta.html", {'form': form, 'formset_respuestas': formset_respuestas})
            else:
                messages.error(request, 'Ha ocurrido un error al guardar la información, por favor vuelva a intentar.')
                return render(request, "modificar_pregunta.html", {'form': form, 'formset_respuestas': formset_respuestas})
        except Exception as e:
            messages.error(request, 'Ha ocurrido un error al guardar la información, por favor vuelva a intentar.')
            return render(request, "modificar_pregunta.html", {'form': form, 'formset_respuestas': formset_respuestas})

    return render(request, "modificar_pregunta.html", {'form': form, 'formset_respuestas': formset_respuestas})


@login_required
def consultar_preguntas(request):
    microservicio = MicroService.objects.get(name="api")
    try:
        respuesta = microservicio.remote_call(
                "GET", api='api/examenes/preguntas', request=request
        )
    except Exception as e:
        messages.error(request, 'Ha ocurrido un error, por favor vuelva a intentar.')
        return redirect('inicio')

    if respuesta.status_code == 200:
        preguntas = respuesta.json()
    else:
        messages.error(request, 'Ha ocurrido un error, por favor vuelva a intentar.')
        return redirect('inicio')

    return render(request, "lista_preguntas.html", {'lista_preguntas': preguntas})


@login_required
def detalle_pregunta(request, pk):
    microservicio = MicroService.objects.get(name="api")
    try:
        respuesta = microservicio.remote_call(
                "GET", api='api/examenes/preguntas/%s' % pk, request=request
        )
    except Exception:
        messages.error(request, 'Ha ocurrido un error, por favor vuelva a intentar.')
        return redirect('examenes:consultar_preguntas')

    if respuesta.status_code == 200:
        pregunta = respuesta.json()
    else:
        messages.error(request, "Ha ocurrido un error, por favor vuelva a intentar.")
        return redirect('examenes:consultar_preguntas')

    return render(request, "detalle_pregunta.html", {'pregunta': pregunta})


@login_required
@verificar_rol(roles_permitidos=["Profesor"])
def registrar_examen(request):
    profesor = get_microservice_user(request)
    form = ExamenForm(profesor=profesor)
    microservicio = MicroService.objects.get(name="api")

    if request.method == 'POST':
        form = ExamenForm(request.POST, profesor=profesor)
        if form.is_valid():
            cleaned_data = form.cleaned_data
            data = {}
            data['titulo'] = cleaned_data['titulo']
            data['profesor'] = profesor[settings.MICROSERVICE_USER_PK_KEY]
            data['grupo'] = cleaned_data['grupo']
            data['fecha_inicio'] = cleaned_data['fecha_inicio'].strftime("%Y-%m-%d %H:%M:%S")
            data['fecha_fin'] = cleaned_data['fecha_fin'].strftime("%Y-%m-%d %H:%M:%S")
            data['preguntas_originales'] = cleaned_data['preguntas_originales']
        else:
            messages.error(request, "Por favor revise los campos en rojo")
            return render(request, "gestionar_examen.html", {'form': form})

        try:
            respuesta = microservicio.remote_call(
                    "POST", api='api/examenes/', json=data, request=request
            )
        except Exception as e:
            messages.error(request, 'Ha ocurrido un error al guardar la información, por favor vuelva a intentar.')
            return redirect('examenes:consultar_examenes')

        if respuesta.status_code == status.HTTP_201_CREATED:
            messages.success(request, "Examen registrado correctamente")
            return redirect('examenes:consultar_examenes')
        elif respuesta.status_code == status.HTTP_400_BAD_REQUEST:
            errores = respuesta.json()
            add_form_errors(form, errores)
            messages.error(request, "Por favor revise los campos en rojo")
            return render(request, "gestionar_examen.html", {'form': form})
        else:
            messages.error(request, 'Ha ocurrido un error al guardar la información, por favor vuelva a intentar.')
            return redirect('examenes:consultar_examenes')

    return render(request, "gestionar_examen.html", {'form': form})


@login_required
@verificar_rol(roles_permitidos=["Profesor"])
def modificar_examen(request, pk):
    profesor = get_microservice_user(request)
    microservicio = MicroService.objects.get(name="api")

    try:
        respuesta = microservicio.remote_call(
                "GET", api='api/examenes/%s' % pk, request=request
        )
    except Exception:
        messages.error(request, 'Ha ocurrido un error, por favor vuelva a intentar.')
        return redirect('inicio')

    if respuesta.status_code == 200:
        examen = respuesta.json()
    else:
        messages.error(request, 'Error al obtener los datos del examen')
        return redirect('examenes:consultar_preguntas')

    form = ExamenForm(profesor=profesor, examen=examen, initial=dict(examen))

    if request.method == 'POST':
        form = ExamenForm(request.POST, profesor=profesor, examen=examen, initial=dict(examen))
        if form.is_valid():
            cleaned_data = form.cleaned_data
            data = {}
            data['titulo'] = cleaned_data['titulo']
            data['profesor'] = profesor[settings.MICROSERVICE_USER_PK_KEY]
            data['grupo'] = cleaned_data['grupo']
            data['fecha_inicio'] = cleaned_data['fecha_inicio'].strftime("%Y-%m-%d %H:%M:%S")
            data['fecha_fin'] = cleaned_data['fecha_fin'].strftime("%Y-%m-%d %H:%M:%S")
            data['preguntas_originales'] = cleaned_data['preguntas_originales']
        else:
            messages.error(request, "Por favor revise los campos en rojo")
            return render(request, "gestionar_examen.html", {'form': form})
        try:
            respuesta = microservicio.remote_call(
                    "POST", api='api/examenes/%s' % pk, json=data, request=request
            )
        except Exception as e:
            messages.error(request, 'Ha ocurrido un error al guardar la información, por favor vuelva a intentar.')
            return redirect('examenes:consultar_examenes')

        if respuesta.status_code == status.HTTP_200_OK:
            messages.success(request, "Examen actualizado correctamente")
            return redirect('examenes:consultar_examenes')
        elif respuesta.status_code == status.HTTP_400_BAD_REQUEST:
            errores = respuesta.json()
            form = ExamenForm(request.POST, profesor=profesor, examen=examen, initial=examen)
            add_form_errors(form, errores)
            messages.error(request, "Por favor revise los campos en rojo")
            return render(request, "gestionar_examen.html", {'form': form})
        else:
            messages.error(request, 'Ha ocurrido un error al guardar la información, por favor vuelva a intentar.')
            return redirect('examenes:consultar_examenes')

    return render(request, "gestionar_examen.html", {'form': form})


@login_required
def consultar_examenes(request):
    microservicio = MicroService.objects.get(name="api")
    try:
        respuesta = microservicio.remote_call(
                "GET", api='api/examenes/', request=request
        )
    except Exception as e:
        messages.error(request, 'Ha ocurrido un error, por favor vuelva a intentar.')
        return redirect('inicio')

    if respuesta.status_code != 200:
        messages.error(request, 'Ha ocurrido un error, por favor vuelva a intentar.')
        return redirect('inicio')

    examenes = respuesta.json()
    try:
        respuesta = microservicio.remote_call(
                "GET", api='api/grupos/', request=request
        )
    except Exception as e:
        grupos = []

    if respuesta.status_code == 200:
        grupos = respuesta.json()
    else:
        grupos = []

    for examen in examenes:
        for grupo in grupos:
            if grupo['id'] == examen['grupo']:
                examen['grupo'] = grupo

    return render(request, "lista_examenes.html", {'lista_examenes': examenes})


@login_required
def consultar_examenes_estudiante(request):
    microservicio = MicroService.objects.get(name="api")
    try:
        respuesta = microservicio.remote_call(
                "GET", api='api/examenes/', request=request
        )
    except Exception as e:
        messages.error(request, 'Ha ocurrido un error, por favor vuelva a intentar.')
        return redirect('inicio')

    if respuesta.status_code != 200:
        messages.error(request, 'Ha ocurrido un error, por favor vuelva a intentar.')
        return redirect('inicio')

    examenes = respuesta.json()
    try:
        respuesta = microservicio.remote_call(
                "GET", api='api/grupos/', request=request
        )
    except Exception as e:
        grupos = []

    if respuesta.status_code == 200:
        grupos = respuesta.json()
    else:
        grupos = []

    for examen in examenes:
        for grupo in grupos:
            if grupo['id'] == examen['grupo']:
                examen['grupo'] = grupo

    return render(request, "lista_examenes_estudiante.html", {'lista_examenes': examenes})


@login_required
def detalle_examen(request, pk):
    microservicio = MicroService.objects.get(name="api")
    try:
        respuesta = microservicio.remote_call(
                "GET", api='api/examenes/%s' % pk, request=request
        )
    except Exception:
        messages.error(request, 'Ha ocurrido un error, por favor vuelva a intentar.')
        return redirect('examenes:consultar_examenes')

    if respuesta.status_code == 200:
        examen = respuesta.json()
    else:
        messages.error(request, "Ha ocurrido un error, por favor vuelva a intentar.")
        return redirect('examenes:consultar_examenes')

    try:
        respuesta = microservicio.remote_call(
                "GET", api='api/grupos/%s' % examen['grupo'], request=request
        )

        if respuesta.status_code == 200:
            examen['grupo'] = respuesta.json()
        else:
            examen['grupo'] = {}
    except Exception as e:
        examen['grupo'] = {}

    return render(request, "detalle_examen.html", {'examen': examen})


@login_required
def iniciar_examen(request, pk):
    microservicio = MicroService.objects.get(name="api")
    try:
        respuesta = microservicio.remote_call(
                "GET", api='api/examenes/iniciar-examen/%s' % pk, request=request
        )
    except Exception:
        messages.error(request, 'Ha ocurrido un error, por favor vuelva a intentar.')
        return redirect('examenes:consultar_examenes')

    if respuesta.status_code == 200:
        messages.success(request, "Examen iniciado correctamente.")
    else:
        messages.error(request, "Ha ocurrido un error, por favor vuelva a intentar.")

    return redirect('examenes:consultar_examenes')


@login_required
def terminar_examen(request, pk):
    microservicio = MicroService.objects.get(name="api")
    try:
        respuesta = microservicio.remote_call(
                "GET", api='api/examenes/terminar-examen/%s' % pk, request=request
        )
    except Exception:
        messages.error(request, 'Ha ocurrido un error, por favor vuelva a intentar.')
        return redirect('examenes:consultar_examenes')

    if respuesta.status_code == 200:
        messages.success(request, "Examen terminado correctamente.")
    else:
        messages.error(request, "Ha ocurrido un error, por favor vuelva a intentar.")

    return redirect('examenes:consultar_examenes')


@login_required
@verificar_rol(roles_permitidos=["Estudiante"])
def responder_examen(request, pk):
    microservicio = MicroService.objects.get(name="api")
    try:
        respuesta = microservicio.remote_call(
                "GET", api='api/examenes/responder-examen/%s' % pk, request=request
        )
    except Exception:
        messages.error(request, 'Ha ocurrido un error, por favor vuelva a intentar.')
        return redirect('examenes:consultar_examenes_estudiante')

    if respuesta.status_code == 200:
        examen = respuesta.json()
    else:
        messages.error(request, "Ha ocurrido un error, por favor vuelva a intentar.")
        return redirect('examenes:consultar_examenes_estudiante')

    if examen.get('terminado') == True:
        messages.warning(request, "Ha terminado el examen y ya no es posible editarlo")
        return redirect('examenes:consultar_examenes_estudiante')

    preguntas = examen['preguntas']
    total_preguntas = len(preguntas)
    PreguntaResueltaFormSet = formset_factory(form=PreguntaResueltaForm, formset=BasePreguntasFormSet, extra=total_preguntas, max_num=total_preguntas)
    formset_preguntas = PreguntaResueltaFormSet(initial=preguntas, form_kwargs={'lista_preguntas': preguntas})

    if request.method == "POST":
        formset_preguntas = PreguntaResueltaFormSet(request.POST, initial=preguntas, form_kwargs={'lista_preguntas': preguntas})
        if request.is_ajax():
            if formset_preguntas.is_valid():
                data = {}
                data['preguntas'] = formset_preguntas.cleaned_data
                try:
                    respuesta = microservicio.remote_call(
                            "POST", api='api/examenes/responder-examen/%s' % pk, json=data, request=request, headers={'X-Requested-With': 'XMLHttpRequest'}
                    )
                except Exception:
                    return HttpResponseBadRequest()
                if respuesta.status_code == status.HTTP_400_BAD_REQUEST:
                    return HttpResponseBadRequest()
            else:
                return HttpResponseBadRequest()
            return JsonResponse({})
        else:
            if formset_preguntas.is_valid():
                data = {}
                data['preguntas'] = formset_preguntas.cleaned_data
                try:
                    respuesta = microservicio.remote_call(
                            "POST", api='api/examenes/responder-examen/%s' % pk, json=data, request=request
                    )
                except Exception:
                    messages.error(request, "Ha ocurrido un error, por favor vuelva a intentar.")
                    return redirect('examenes:consultar_examenes_estudiante')

                if respuesta.status_code == status.HTTP_200_OK:
                    messages.success(request, "El examen ha sido guardado exitosamente")
                    return redirect('examenes:consultar_examenes_estudiante')
                else:
                    messages.error(request, "Ha ocurrido un error, por favor vuelva a intentar.")
                    return redirect('examenes:consultar_examenes_estudiante')

            else:
                messages.error(request, "El examen no ha sido guardado satisfactoriamente, por favor inténtelo de nuevo")

    return render(request, "responder_examen.html", {'examen': examen, 'formset_preguntas': formset_preguntas})


@login_required
def consultar_examenes_resueltos_estudiante(request):
    microservicio = MicroService.objects.get(name="api")
    try:
        respuesta = microservicio.remote_call(
                "GET", api='api/examenes/examenes-estudiante', request=request
        )
    except Exception as e:
        messages.error(request, 'Ha ocurrido un error, por favor vuelva a intentar.')
        return redirect('inicio')

    if respuesta.status_code != 200:
        messages.error(request, 'Ha ocurrido un error, por favor vuelva a intentar.')
        return redirect('inicio')

    examenes = respuesta.json()
    try:
        respuesta = microservicio.remote_call(
                "GET", api='api/grupos/', request=request
        )
    except Exception as e:
        grupos = []

    if respuesta.status_code == 200:
        grupos = respuesta.json()
    else:
        grupos = []

    for examen in examenes:
        for grupo in grupos:
            if grupo['id'] == examen['grupo']:
                examen['grupo'] = grupo

    return render(request, "lista_examenes_resueltos_estudiante.html", {'lista_examenes': examenes})


@login_required
def ver_calificacion(request, pk):
    microservicio = MicroService.objects.get(name="api")
    try:
        respuesta = microservicio.remote_call(
                "GET", api='api/examenes/calificacion-examen/%s' % pk, request=request
        )
    except Exception:
        messages.error(request, 'Ha ocurrido un error, por favor vuelva a intentar.')
        return redirect(request.META.get('HTTP_REFERER', 'inicio'))

    if respuesta.status_code == 200:
        examen = respuesta.json()
    else:
        messages.error(request, "Ha ocurrido un error, por favor vuelva a intentar.")
        return redirect(request.META.get('HTTP_REFERER', 'inicio'))

    try:
        respuesta = microservicio.remote_call(
                "GET", api='api/grupos/%s' % examen['grupo'], request=request
        )

        if respuesta.status_code == 200:
            examen['grupo'] = respuesta.json()
        else:
            examen['grupo'] = {}
    except Exception as e:
        examen['grupo'] = {}

    return render(request, "ver_calificacion.html", {'examen': examen})


@login_required
def consultar_calificaciones_examen(request, pk):
    microservicio = MicroService.objects.get(name="api")
    try:
        respuesta = microservicio.remote_call(
                "GET", api='api/examenes/calificacions-examen/%s' % pk, request=request
        )
    except Exception:
        messages.error(request, 'Ha ocurrido un error, por favor vuelva a intentar.')
        return redirect('examenes:consultar_examenes')

    if respuesta.status_code == 200:
        examen = respuesta.json()
    else:
        messages.error(request, "Ha ocurrido un error, por favor vuelva a intentar.")
        return redirect('examenes:consultar_examenes')

    try:
        respuesta = microservicio.remote_call(
                "GET", api='api/grupos', request=request
        )

        if respuesta.status_code == 200:
            grupos = respuesta.json()
        else:
            grupos = []
    except Exception as e:
        grupos = []

    for examen in examen['examenes_resueltos']:
        for grupo in grupos:
            if grupo['id'] == examen['grupo']:
                examen['grupo'] = grupo

    return render(request, "lista_calificaciones_examen.html", {'examen': examen})
